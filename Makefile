genctags:
	@@ctags -R --extra=+f --exclude=.git

pep8:
	@@autoflake -ri --remove-all-unused-imports .
	@@autopep8 -ri --max-line-length 130 abacus

clear:
	@@find . -name '*.pyc' | xargs rm
	@@find . -name '__pycache__' | xargs rm -rf
