from distutils.core import setup

REQUIREMENTS = [i.strip() for i in open("requirements.txt").readlines()]

setup(
    name='abacus',
    version='0.2.0',
    author='Rit Li',
    author_email='rit@quietdynamite.com',
    packages=['abacus', 'abacus.test'],
    url='https://bitbucket.org/rit/abacus',
    license='http://opensource.org/licenses/MIT',
    description='Helper Library for Tornado Web Framework',
    install_requires=REQUIREMENTS,
    long_description=open('README.rst').read(),
    classifiers=[
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: Implementation :: CPython'
    ],
)
