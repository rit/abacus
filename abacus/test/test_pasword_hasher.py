from mock import patch
from pytest import mark

from abacus.util import password_hasher


@mark.slowtest
def test_password_hasher_generate_hash(password):
    with patch("abacus.util.bcrypt.gensalt") as gensalt:
        gensalt.return_value = password.salt
        hash = password_hasher(password.plaintext)
        assert hash == password.hash


@mark.slowtest
def test_password_hasher_with_hash(password):
    hash = password_hasher(password.plaintext, password.hash)
    assert hash == password.hash
