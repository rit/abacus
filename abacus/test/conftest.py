from collections import namedtuple
from datetime import datetime

import pytest
import pytz


from abacus.db import Session


@pytest.fixture
def dbsession(request):

    dbsession = Session()
    dbsession.begin_nested()

    def rollback():
        dbsession.rollback()
        dbsession.close()

    request.addfinalizer(rollback)
    return dbsession


def _pw_hash(pw):
    if pw == "dragon":
        return "$2a$12$WZ5gEu36V57GrVdHBugNielRoOpS7ncFy5W7N6ZnkWoiKz7QeJ/Ay"


Password = namedtuple("Password", "plaintext salt hash")


@pytest.fixture
def password(request):
    pw = Password(
        plaintext='dragon',
        salt='$2a$12$WZ5gEu36V57GrVdHBugNie',
        hash='$2a$12$WZ5gEu36V57GrVdHBugNielRoOpS7ncFy5W7N6ZnkWoiKz7QeJ/Ay'
    )
    return pw


@pytest.fixture
def la_noon():
    la = pytz.timezone("America/Los_Angeles")
    ts = datetime(2013, 12, 15, 12, tzinfo=la)
    return ts


@pytest.fixture
def la_evening():
    la = pytz.timezone("America/Los_Angeles")
    ts = datetime(2013, 12, 15, 18, tzinfo=la)
    return ts
