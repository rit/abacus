from urllib import urlencode
import json

from tornado.web import RequestHandler

from abacus.handlers import SimpleHandler
from abacus.handlers import extract_args
from abacus.testing import BaseTestHandler


class ExtractHandler(RequestHandler):

    def get(self):
        self.finish('ok')

    def post(self):
        attrs = extract_args(self.request, 'email', 'name', 'hello')
        self.finish(attrs)


class SampleSimpleHandler(SimpleHandler):

    def get(self):
        self.render('simple.html')


class BaseTest(BaseTestHandler):

    def get_template_path(self):
        return 'abacus/test/templates'


class TestExtractData(BaseTest):
    url = '/data'
    handler = ExtractHandler

    def test_extract_args_urlencode(self):
        payload = {
            "email": "rit@xap.me",
            "name": "Rit Li"
        }
        res = self.post(self.url, body=urlencode(payload))
        assert res.code == 200
        assert json.loads(
            res.body) == {'email': 'rit@xap.me', 'name': "Rit Li"}

    def test_extract_args_body_json(self):
        payload = {
            "email": "rit@xap.me",
            "name": "Rit Li"
        }
        res = self.post(self.url, body=json.dumps(payload))
        assert res.code == 200
        assert json.loads(
            res.body) == {'email': 'rit@xap.me', 'name': "Rit Li"}


class TestSimpleHandler(BaseTest):
    url = '/url'
    handler = SampleSimpleHandler

    def test_custom_namespaces(self):
        res = self.fetch(self.url)
        assert res.code == 200


# class TestRestHandler:
#     url = resturl('/foo/:id/bar/:id/baz/:id')
#     handler = RestHandler
#
#     def test_get_index(self):
#         res = self.fetch('/foo/1/bar/2/baz')
#         assert res.code == 200
#         assert res.body == 'index'
#
#     def test_get_show(self):
#         res = self.fetch('/foo/1/bar/2/baz/3')
#         assert res.code == 200
#         assert res.body == 'show'
#
#     def test_get_new(self):
#         res = self.fetch('/foo/1/bar/2/baz/new')
#         assert res.code == 200
#         assert res.body == 'new'
#
#     def test_get_edit(self):
#         res = self.fetch('/foo/1/bar/2/baz/3/edit?q=hello')
#         assert res.code == 200
#         assert res.body == 'edit'
