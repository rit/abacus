from abacus.util import resturl
from abacus.util import nexturl

pattern = '[a-zA-Z0-9_]'


def test_simple_restrul():
    assert resturl('/games/:id') == r'/games(?:/([a-zA-Z0-9_]+)(?:/edit)?)?$'


def test_nested_resturl():
    assert resturl('/games/:id/replies/:id') == r'/games/([a-zA-Z0-9_]+)/replies(?:/([a-zA-Z0-9_]+)(?:/edit)?)?$'


def test_more_nested_resturl():
    assert resturl('/a/:id/b/:id/c/:id') == r'/a/([a-zA-Z0-9_]+)/b/([a-zA-Z0-9_]+)/c(?:/([a-zA-Z0-9_]+)(?:/edit)?)?$'


def test_nexturl_index():
    resource_url = '/resources/:id'
    url = nexturl(resource_url)
    assert url == '/resources'


def test_nexturl_index_nested():
    resource_url = '/foo/:id/bar/:id/baz/:id'
    url = nexturl(resource_url, 50, 20)
    assert url == '/foo/50/bar/20/baz'


def test_nexturl_new():
    resource_url = '/resources/:id'
    url = nexturl(resource_url, new=True)
    assert url == '/resources/new'


def test_nexturl_show():
    resource_url = '/resources/:id'
    url = nexturl(resource_url, '1')
    assert url == '/resources/1'


def test_nexturl_show_nested():
    resource_url = '/foo/:id/bar/:id/baz/:id'
    url = nexturl(resource_url, 50, 20, 30)
    assert url == '/foo/50/bar/20/baz/30'


def test_nexturl_edit_nested():
    resource_url = '/foo/:id/bar/:id/baz/:id'
    url = nexturl(resource_url, 50, 20, 30, edit=True)
    assert url == '/foo/50/bar/20/baz/30/edit'
