import Cookie
import json

from tornado.web import RequestHandler
from tornado.web import decode_signed_value

from abacus.testing import BaseTestHandler
from abacus.ui import AlertUI
from abacus.ui import AlertMixin


class Handler(RequestHandler, AlertMixin):

    def get(self):
        self.alert_success('foo')
        self.alert_success('bar')
        self.alert_error('stranger danger')
        self.serialize_alert_messages()
        super(Handler, self).flush()
        self.render('ui.html')


class Base(BaseTestHandler):

    def get_template_path(self):
        return 'abacus/test/templates'

    def get_ui_modules(self):
        return dict(alerts=AlertUI)


class TestAlertUI(Base):
    url = '/ui'
    handler = Handler

    def test_get_alert_messages(self):
        res = self.fetch(self.url)
        assert res.code == 200
        cookies = res.headers.get_list('set-cookie')
        assert cookies
        cookie = Cookie.SimpleCookie()
        alerts = cookies[0]
        cookie.load(alerts)
        value = cookie['_alert_messages'].value
        msg = decode_signed_value('secret', '_alert_messages', value)
        data = json.loads(msg)
        assert data['success'] == ['foo', 'bar']
        assert data['error'] == ['stranger danger']
