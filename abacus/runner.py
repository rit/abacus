import logging
import sys
import signal

from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.options import define
from tornado.options import options
from tornado.options import parse_config_file


def parse_options():
    define('shared_secrets', default='shared_secrets.cfg')
    define('port', default=7100, type=int, help="Run on port")

    # 'abacus' group
    define('prefix_url', default=None, type=str,
           help="URL prefix for NGINX rewrite url", group='abacus')
    define('debug', default=False, type=bool,
           help="Enable debug mode", group='abacus')
    define('cookie_secret', default='todo-set-super-secret',
           type=str, help="Cookie Secret", group='abacus')

    parse_config_file(options.shared_secrets)


def shutdown(sig, frame):
    server.stop()
    IOLoop.instance().stop()
    sys.exit()


def run(app):
    global server
    server = HTTPServer(app)
    server.listen(options.port)

    # handle Ctrl-C from the terminal
    signal.signal(signal.SIGINT, shutdown)

    # handle "kill -USR1 $PID"
    # use lsof -i :PORT_NUM to find the pid
    signal.signal(signal.SIGUSR1, shutdown)

    logging.info("Listening on port: %d" % options.port)
    IOLoop.instance().start()
