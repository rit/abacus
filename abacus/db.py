from datetime import datetime

from pytz import utc
from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker

from sqlalchemy import Table, MetaData
from sqlalchemy import event
from sqlalchemy import types
from sqlalchemy.ext.declarative import declarative_base

from abacus.util import load_config

config = load_config('config')

engine = create_engine(config["dburl"], echo=False)
Session = sessionmaker(bind=engine)

metadata = MetaData(bind=engine)
Base = declarative_base()


@event.listens_for(Table, "column_reflect")
def set_utc_date(inspector, table, column_info):
    if isinstance(column_info['type'], types.DateTime):
        column_info['type'] = UTCDateTime()


class UTCDateTime(types.TypeDecorator):

    impl = types.DateTime

    def process_bind_param(self, value, engine):
        if isinstance(value, datetime):
            return value.astimezone(utc)
        raise "value must be datetime object"

    def process_result_value(self, value, engine):
        if value is not None:
            return value.replace(tzinfo=utc)
