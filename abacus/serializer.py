import json
from datetime import datetime


class ModelEncoder(json.JSONEncoder):

    def default(self, obj):
        if hasattr(obj, '__mapper__'):
            return {k: getattr(obj, k) for k in obj.__mapper__.c.keys()}
        if isinstance(obj, datetime):
            return obj.isoformat()
        return json.JSONEncoder.default(self, obj)


def jsonify(obj):
    return json.dumps(obj, cls=ModelEncoder)
