from datetime import datetime
from uuid import uuid4

from mock import Mock
from mock import patch
from tornado.testing import AsyncHTTPTestCase
from tornado.web import Application
from tornado.web import RequestHandler
from tornado.web import UIModule
import pytz

from tornado.web import UIModule
import pytz

from abacus.db import Session
from abacus.db import engine


class MockUIModule(UIModule):

    def render(self):
        return 'mock_ui_module'


class MockHandler(RequestHandler):

    def get(self):
        self.write('Hello, World!')


class BaseTestHandler(AsyncHTTPTestCase):

    """
    use these class attributes to setup tesst:
    - url
    - handler
    - options - dictionary passed into 'initialize'
    """

    _headers = {'content-type': 'application/x-www-form-urlencoded'}

    def get_app(self):
        assert self.url
        assert self.handler

        opts = self.app_options()
        app = Application(
            [
                (self.url, self.handler, opts),
                ('/mock_url', MockHandler)
            ],
            cookie_secret='secret',
            _testing=True,
            ui_modules=self.get_ui_modules(),
            template_path=self.get_template_path(),
        )
        app.reverse_url = Mock(side_effect=lambda _: '/mock_url')
        return app

    def put(self, url, body=None, **kwargs):
        return self.do_request(url, method='PUT', body=body, **kwargs)

    def post(self, url, body=None, **kwargs):
        return self.do_request(url, method='POST', body=body, **kwargs)

    def delete(self, url, **kwargs):
        return self.fetch(url, method='DELETE', headers=self._headers, **kwargs)

    def do_request(self, url, body=None, method='GET', follow_redirects=False, **kwargs):
        if body is None:
            body = ''
        return self.fetch(url, method=method, body=body, headers=self._headers,
                          follow_redirects=follow_redirects, **kwargs)

    def get_template_path(self):
        raise NotImplementedError

    def get_ui_modules(self):
        return {}

    def app_options(self):
        return {}


class CrudTestHandler(BaseTestHandler):

    def tearDown(self):
        self.transaction.rollback()
        self.dbsession.close()
        self.connection.close()

        super(BaseTestHandler, self).tearDown()

    def app_options(self):
        self.connection = engine.connect()
        self.transaction = self.connection.begin()
        self.dbsession = Session(bind=self.connection)

        opts = getattr(self, 'options',  {})
        opts.update({'dbsession': self.dbsession})
        return opts


def dbsession(request):
    dbsession = Session()
    dbsession.begin_nested()

    def rollback():
        dbsession.rollback()

    request.addfinalizer(rollback)
    return dbsession


def morning():
    now = datetime.now()
    zone = pytz.timezone('America/Los_Angeles')
    dt = zone.localize(datetime(now.year, now.month, now.day, 5))
    return dt


def evening():
    now = datetime.now()
    zone = pytz.timezone('America/Los_Angeles')
    dt = zone.localize(datetime(now.year, now.month, now.day, 19))
    return dt


def uuid():
    return str(uuid4())


def current_user_guid(request):
    guid = str(uuid4())
    user = lambda _: {'user_guid': guid}
    patcher = patch.object(request.cls.handler, 'get_current_user', user)

    cleanup = lambda: patcher.stop()

    request.addfinalizer(cleanup)
    patcher.start()
    return guid
