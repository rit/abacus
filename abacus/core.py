from threading import Lock


class Cache(object):

    def __init__(self):
        self.cache = {}
        self.lock = Lock()

    def __getitem__(self, key):
        with self.lock:
            return self.cache[key]

    def __setitem__(self, key, value):
        with self.lock:
            self.cache[key] = value

    def __contains__(self, item):
        return item in self.cache
