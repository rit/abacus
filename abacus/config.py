
from abacus.util import load_config

config = load_config("config")
# TODO merge config with config passed via CLI options


def asset_url(asset):
    return config["static_host"] + asset
