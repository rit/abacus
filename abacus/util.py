from itertools import count
import re
import os

import bcrypt
import yaml

from abacus.core import Cache


def resturl(url):
    url = re.sub(r'/:id$', '(?:/([a-zA-Z0-9_]+)(?:/edit)?)?$', url)
    url = re.sub(r'/:id', '/([a-zA-Z0-9_]+)', url)
    return url


def nexturl(url, *args, **kwargs):
    args_count = len(args)
    kwargs_count = len(kwargs)
    if args_count == 0 and kwargs_count == 0:
        return re.sub(r'/:id$', '', url)

    pattern = re.compile(':id')
    id_count = len(pattern.findall(url))
    if args_count == id_count - 1:
        # url for index /foo/:id/bar
        url = re.sub(r'/:id$', '', url)

    idx = count()
    value = lambda _: str(args[next(idx)])
    url = pattern.sub(value, url)

    if kwargs.get('new'):
        url = url + '/new'
    elif kwargs.get('edit'):
        url = url + '/edit'
    return url


def password_hasher(pw, hashed=None):
    if not hashed:
        hashed = bcrypt.gensalt()

    return bcrypt.hashpw(pw, hashed)


_config_cache = Cache()


def load_config(fname):
    mode = os.getenv("APP_MODE", "dev")
    name = "{}_{}.yml".format(fname, mode)
    if name in _config_cache:
        return _config_cache[name]
    else:
        path = os.path.join(os.getcwd(), name)
        with open(path) as f:
            _config_cache[name] = yaml.load(f, yaml.CLoader)
    return _config_cache[name]
