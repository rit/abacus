from datetime import datetime
import json

import pytz
from tornado.web import RequestHandler
from tornado.web import HTTPError

from abacus.config import asset_url
from abacus.util import nexturl
from abacus.ui import AlertMixin


AUTHENTICATED_USER_COOKIE_NAME = "current_user"


def extract(data, *keys):
    return {key: data.get(key) for key in keys if data.get(key, None)}


def extract_args(request, *keys):
    output = {}
    for key, value in request.body_arguments.items():
        if key in keys:
            if len(value) == 1:
                output[key] = value[0]
            else:
                output[key] = value

    try:
        data = json.loads(request.body)
        for key in data:
            if key in keys:
                output[key] = data[key]
    except ValueError:
        pass
    return output

dtz = ('_date', '_time', '_tz')


def parse_datetime_field(handler, key):
    keys = [key + e for e in dtz]
    date, time, zone = [handler.get_argument(key) for key in keys]
    tz = pytz.timezone(zone)
    if ':' in time:
        fmt = "%Y-%m-%d %I:%M%p"
    else:
        fmt = "%Y-%m-%d %I%p"
    dt = datetime.strptime("{} {}".format(date, time), fmt)
    return tz.localize(dt)


class SimpleHandler(RequestHandler, AlertMixin):

    def __init__(self, *args, **kwargs):
        super(SimpleHandler, self).__init__(*args, **kwargs)

    def initialize(self):
        self.prefix_url = self.settings.get("prefix_url")
        self._testing = self.settings.get("_testing", False)

    def error(self, code, body):
        self.set_status(code)
        self.finish(body)

    def get_current_user(self):
        data = self.get_secure_cookie(AUTHENTICATED_USER_COOKIE_NAME)
        if not data:
            return None

        return json.loads(data)

    def user_guid(self):
        return self.get_current_user()["user_guid"]

    def nexturl(self, url, *args, **kwargs):
        url = nexturl(url, *args, **kwargs)
        if self.prefix_url:
            url = self.prefix_url + url
        return url

    def prefix_reverse_url(self, name, *args):
        url = self.reverse_url(name, *args)
        if self.prefix_url:
            return "{0}{1}".format(self.prefix_url, str(url))
        return url

    def get_template_namespace(self):
        namespace = super(SimpleHandler, self).get_template_namespace()
        namespace.update({
            'prefix_reverse_url': self.prefix_reverse_url,
            'nexturl': self.nexturl,
            'asset_url': asset_url
        })
        return namespace


class CrudHandler(SimpleHandler):

    def __init__(self, *args, **kwargs):
        super(CrudHandler, self).__init__(*args, **kwargs)

    def initialize(self, dbsession=None):
        super(CrudHandler, self).initialize()
        self.dbsession = dbsession or self.settings["dbsession_factory"]()

    def on_finish(self):
        if not self._testing:
            self.dbsession.close()

    def flush(self, include_footers=False, callback=None):
        self.serialize_alert_messages()

        super(CrudHandler, self).flush(include_footers, callback)


class RestHandler(CrudHandler):

    def get(self, *args):
        uid = args[-1]
        if uid is None:
            self.index(*args)
        elif self.request.path.endswith('edit'):
            self.edit(*args)
        elif uid == 'new':
            self.new(*args)
        else:
            self.show(*args)

    def post(self, *args):
        method = self.get_argument('_method', None)
        if method:
            meth = getattr(self, method.lower())
            meth(*args)
        else:
            self.create(*args)

    def put(self, *args):
        self.update(*args)

    def delete(self, *args):
        self.destroy(*args)

    def index(self, *args):
        raise HTTPError(405)

    def show(self, *args):
        raise HTTPError(405)

    def new(self, *args):
        raise HTTPError(405)

    def edit(self, *args):
        raise HTTPError(405)

    def create(self, *args):
        raise HTTPError(405)

    def update(self, *args):
        raise HTTPError(405)

    def destroy(self, *args):
        raise HTTPError(405)
