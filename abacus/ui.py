import Cookie
import json
import os
from datetime import datetime
from datetime import timedelta

from tornado.web import UIModule
from tornado.template import Template


def daypicker(count, now=None):
    out = []
    if not now:
        now = datetime.now() - timedelta(days=1)
    for i in xrange(count):
        day = now + timedelta(days=i)
        out.append((day.strftime('%Y-%m-%d'), day.strftime('%A, %B %d')))
    return out


class LocalUIModule(UIModule):

    _template = None

    def render_string_local(self, template_name, **kwargs):

        if not self._template:
            path = os.path.join(
                os.path.dirname(__file__), 'templates', template_name)
            with open(path, 'rb') as f:
                self._template = Template(f.read(), template_name)

        return self._template.generate(**kwargs)


class SelectUI(LocalUIModule):

    def render(self, choices, name, required=True):
        html = self.render_string_local('select.html', name=name, choices=choices, required=required)
        return html


class AlertUI(LocalUIModule):

    def render(self):
        data = self.handler.get_secure_cookie(
            AlertMixin.ALERT_MESSAGES_COOKIE_NAME) or '{}'
        alerts = json.loads(data)
        html = self.render_string_local('alerts.html', alerts=alerts)
        self.handler.clear_cookie(AlertMixin.ALERT_MESSAGES_COOKIE_NAME)
        return html


class AlertMixin(object):
    ALERT_MESSAGES_COOKIE_NAME = '_alert_messages'

    def alert_error(self, msg):
        self._alert_msg('error', msg)

    def alert_success(self, msg):
        self._alert_msg('success', msg)

    def serialize_alert_messages(self):
        if hasattr(self, '_alert_messages'):
            self.set_secure_cookie(
                self.ALERT_MESSAGES_COOKIE_NAME, json.dumps(self._alert_messages))

    def _alert_msg(self, level, msg):
        if not hasattr(self, '_alert_messages'):
            self._alert_messages = {}
            if not hasattr(self, '_new_cookie'):
                self._new_cookie = Cookie.SimpleCookie()

        messages = self._alert_messages.setdefault(level, [])
        messages.append(msg)
